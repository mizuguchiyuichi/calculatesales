package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 賞品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_ERROR_FORMAT = "ファイルのフォーマットが不正です";
	private static final String FILE_BRANCH = "支店";
	private static final String FILE_COMMODITYT = "商品";
	private static final String NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String MAX_DIGIT_OVER = "合計金額が10桁を超えました";
	private static final String BAD_FORMAT_CODE = "のフォーマットが不正です";
	private static final String BAD_BRANCH_CODE = "の支店コードが不正です";

	//正規表現
	private static final String SERIAL_JUDGEMENT = "^[0-9]{3}$";
	private static final String COMMODITY_JUGEMENT = "^[0-9a-zA-Z]{8}$";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//コマンドライン引数が渡されているか判定
		if (args.length != 1) {

			//エラー表示
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		//args[] = コマンドライン引数
		//※コマンドライン…外部から与えられる引数
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,
				SERIAL_JUDGEMENT, FILE_BRANCH)) {

			//ファイルが読み込めなかったらreturnでメインメソッドを終了する。
			return;
		}

		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,
				COMMODITY_JUGEMENT, FILE_COMMODITYT)) {

			// ファイルが読み込めなかったらreturnでメインメソッドを終了する。
			return;
		}

		//これからやること↓
		//全ファイルのパス取得しfileの中に格納
		File[] files = new File(args[0]).listFiles();

		//情報ファイルリストを作成
		List<File> rcdFiles = new ArrayList<>();

		//拡張子も含めたファイル名の取得
		for (int i = 0; i < files.length; i++) {

			//売り上げファイルかの判定
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}\\.rcd$")) {

				//売り上げファイルをListに格納
				rcdFiles.add(files[i]);
			}
		}

		//売り上げファイルのソート
		Collections.sort(rcdFiles);

		//売上ファイルが連番かの判定
		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			//先頭から8文字をint型に変換
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			//比較するための次のファイル
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			//連番かどうかの比較
			if ((latter - former) != 1) {

				//エラー表示
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}

		//バッファ初期化
		BufferedReader br = null;

		//ファイルの数だけ繰り返す
		for (int i = 0; i < rcdFiles.size(); i++) {

			//情報格納用リストの作成(for文が回るたびに内部が初期化される)
			List<String> saleData = new ArrayList<>();

			try {

				//ファイル内のデータを呼び出し
				//ファイルリーダー
				FileReader fr = new FileReader(rcdFiles.get(i));

				//バッファードリーダー
				br = new BufferedReader(fr);

				//ファイル内の行データを一時保管
				String line = null;

				//1列ずつリストを読み込みnullになるまで繰り返す
				while ((line = br.readLine()) != null) {

					//リストに追加
					saleData.add(line);
				}

				//売上ファイルのフォーマットを確認する
				if (saleData.size() != 3) {

					//エラー表示
					System.out.println(rcdFiles.get(i).getName() + BAD_FORMAT_CODE);
					return;
				}

				//Mapに特定のKyeが存在するか判定(saleFiles.get(0)にKyeが保管されている)
				if (!branchNames.containsKey(saleData.get(0))) {

					//エラー表示
					System.out.println(rcdFiles.get(i).getName() + BAD_BRANCH_CODE);
					return;
				}

				//Mapに特定のKyeが存在するか判定(saleFiles.get(0)にKyeが保管されている)
				if (!commodityNames.containsKey(saleData.get(0))) {

					//エラー表示
					System.out.println(rcdFiles.get(i).getName() + BAD_BRANCH_CODE);
					return;
				}

				//売上金額が数字なのかを判定
				if (!saleData.get(2).matches("^[0-9]+$")) {

					//エラー表示
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//Listに格納された売上金額データは文字データのためキャストする
				long fileSale = Long.parseLong(saleData.get(2));

				//元々格納されている売上金額と読み込んだ金額を加算しsaleLineに代入する
				//※マップへ参照する際はプリミティブ型ではなく参照型
				Long saleAmount = branchSales.get(saleData.get(0)) + fileSale;
				// 商品別
				Long saleTotal = commoditySales.get(saleData.get(1)) + fileSale;

				//売上金額が11桁以上の場合
				if (saleAmount >= 10000000000L || saleTotal >= 10000000000L) {

					//エラー表示
					System.out.println(MAX_DIGIT_OVER);
					return;
				}

				// mapへ格納(支店別売上ファイル)
				branchSales.put(saleData.get(0), saleAmount);
				// mapへ格納(商品別売上ファイル)
				commoditySales.put(saleData.get(1), saleTotal);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {

				// ファイルを開いている場合
				if (br != null) {
					try {

						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {

			//ファイルが読み込めなかったらreturnで終了する。
			return;
		}

		// 商品別集計ファイルの書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {

			//ファイルが読み込めなかったらreturnで終了する。
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales, String readJudgement, String readErrorMessage) {

		//バッファードリーダーの初期化
		BufferedReader br = null;
		try {

			//ファイル型の作成
			File file = new File(path, fileName);

			//ファイルが存在するかの確認
			if (!file.exists()) {

				System.out.println(readErrorMessage + FILE_NOT_EXIST);
				return false;

			}

			//ファイルリーダー
			FileReader fr = new FileReader(file);

			//バッファードリーダー
			br = new BufferedReader(fr);

			//お金（branchSalesのvalueを初期化）
			long money = 0;

			//変数
			String line;

			// 一行ずつ読み込む
			//nullになるまで繰り返す
			while ((line = br.readLine()) != null) {

				//「","」で区切った文字列をitems[]に格納
				String[] items = line.split(",");

				//「,」で区切ったとき
				// 支店コードファイルの判定
				if ((items.length != 2) || (!items[0].matches(readJudgement))) {

					//エラー表示。
					System.out.println(readErrorMessage + FILE_ERROR_FORMAT);
					return false;
				}

				//itemsに入っているデータをmapに格納
				//配列の「0」に支店コード,「1」に支店名
				branchNames.put(items[0], items[1]);

				//itemsに入っているデータをmapに格納
				branchSales.put(items[0], money);
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {

			// ファイルを開いている場合
			if (br != null) {
				try {

					// ファイルを閉じる
					br.close();
				} catch (IOException e) {

					//エラー表示
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales) {

		//バッファの作成
		BufferedWriter bw = null;

		try {
			//書き込み用ファイルの作成
			File file = new File(path, fileName);

			//バッファから文字列を受け取る
			FileWriter fw = new FileWriter(file);

			//プログラムから文字列を受け取る
			bw = new BufferedWriter(fw);

			//2つのMapから全てのkye情報を取得。
			for (String key : mapSales.keySet()) {

				//書き込み処理(支店コード,支店名,売上金額)
				bw.write(key + "," + mapNames.get(key) + "," + mapSales.get(key));

				//次の行へ
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {

			// ファイルを開いている場合
			if (bw != null) {
				try {

					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
